import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { StationMapComponent } from './station-map.component';
import {Station} from '../models/station';
import {Observable} from 'rxjs/Observable';
import {StationService} from '../services/station-service/station.service';
import {StationInfo} from '../models/station-info';
import {Aqi} from '../models/aqi';

describe('StationMapComponent', () => {
  let stationMapComponent;
  let stationServiceMock;
  let routerMock;
  let fakeStations : Station[];
  let fakeStation1 : Station;
  const mockNgZone = jasmine.createSpyObj('mockNgZone', ['run', 'runOutsideAngular']);
  mockNgZone.run.and.callFake(fn => fn());

    beforeEach(async(() => {
        stationServiceMock = jasmine.createSpyObj('StationService', ['getStations']);
        routerMock = jasmine.createSpyObj('Router', ['getStations']);
        stationMapComponent = new StationMapComponent(stationServiceMock, routerMock, mockNgZone);

        fakeStation1 = createStation(0,0, 25, "green");
        fakeStations = [fakeStation1];
    }));

    it('ngOnInit calls service\'s getStations()', () => {
      // Arrange
      stationServiceMock.getStations.and.returnValue(Observable.of(fakeStations));

      // Act
      stationMapComponent.ngOnInit();

      // Assert
      expect(stationServiceMock.getStations).toHaveBeenCalled();
    });

    it('ngOnInit calls fetches markers', () => {
        // Arrange
        stationServiceMock.getStations.and.returnValue(Observable.of(fakeStations));
        let spy = spyOn(stationMapComponent, 'getMarker');

        // Act
        stationMapComponent.ngOnInit();

        // Assert
        expect(spy).toHaveBeenCalled();
    });

    it('ngOnInit adds a marker for each station', () => {
        // Arrange
        stationServiceMock.getStations.and.returnValue(Observable.of(fakeStations));

        // Act
        stationMapComponent.ngOnInit();

        // Assert
        expect(stationMapComponent.markers.length).toBe(fakeStations.length);
    });
});

function createStation(lat : number, long : number, aqi : number, color : string) : Station{
    let s = new Station();

    s.info = new StationInfo();
    s.aqi = new Aqi();

    s.info.latitude = lat;
    s.info.longitude = long;
    s.aqi.average = aqi;
    s.aqi.color = color;
    return s;
}
