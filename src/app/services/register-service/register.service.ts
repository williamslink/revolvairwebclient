import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RegisterService {
  private apiUrl = environment.apiUrl

  constructor(private http: HttpClient) { }

  register(email: string, name: string, password: string): Observable<Object> {
    const url = `${this.apiUrl}/register`;
    return this.http.post(url, {
      email: email,
      name: name,
      password: password,
    }, {observe: 'body'});
  }
}
