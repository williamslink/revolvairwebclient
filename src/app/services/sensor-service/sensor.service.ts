import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError} from 'rxjs/operators';
import 'rxjs/add/observable/of';
import {Sensor} from '../../models/sensor';
import { environment } from '../../../environments/environment';

@Injectable()
export class SensorService {
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getSensors(stationId: number) : Observable<Sensor[]>{
    const url = `${this.apiUrl}/stations/${stationId}/sensors`;

    return this.http.get<Sensor[]>(url)
      .pipe(
        catchError(this.handleError([]))
      );
  }

  getSensor(stationId:number, sensorId: number) : Observable<Sensor>{
    const url = `${this.apiUrl}/stations/${stationId}/sensors/${sensorId}`;

    return this.http.get<Sensor>(url)
      .pipe(
        catchError(this.handleError(new Sensor))
      );
  }

  getLatestValues(stationId:number, sensorId: number) : Observable<number[]>{
    const url = `${this.apiUrl}/stations/${stationId}/sensors/${sensorId}/readings/latest-values`;

    return this.http.get<number[]>(url)
      .pipe(
        catchError(this.handleError([]))
      );
  }

  private handleError<T> (result?: T) {
    return (): Observable<T> => {
      // Retourner une réponse vide.
      return Observable.of(result as T);
    };
  }
}
